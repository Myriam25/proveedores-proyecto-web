export interface Proveedores{
    codigo? : number,
    razonsocial?: string,
    direccion?: string,
    email?: string,
    id? : number
  }
