import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Proveedores} from '../Interfaces/Proveedores';
import {ProveedoresService} from '../service/proveedores.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})

export class FormularioComponent implements OnInit{
  @Input() proveedorSeleccionado: Proveedores = {
  }as Proveedores;

  constructor(private proveedoresService: ProveedoresService, private activedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.status = this.activedRoute.snapshot.params['id'] == undefined ? "agregar" : "modificar";
    if (this.status == 'modificar') {
      this.status = this.activedRoute.snapshot.params['id'] == undefined ? "agregar" : "modificar";
      this.proveedoresService.getProveedor(this.activedRoute.snapshot.params['id']).subscribe(response => {
        this.proveedorSeleccionado = response;
      })

    }

  }

  msgAlert: Boolean = false;
  msgModificar: Boolean = false;
  msgText: string = "";
  status: string = "";

  agregarProveedores(): void {
    if (this.proveedorSeleccionado.codigo == 0 || this.proveedorSeleccionado.razonsocial == '' || this.proveedorSeleccionado.direccion == ''|| this.proveedorSeleccionado.email == ''|| this.proveedorSeleccionado.codigo == 0) {
      this.msgText = "Los campos están vacíos."
      this.msgAlert = true;
      return;
    }

    this.proveedoresService.agregarProvedor({...this.proveedorSeleccionado}).subscribe(data => {
      console.log(data);

    });

    this.limpiarCajas();
  }


  modificarProveedor() {
    Swal.fire({
      title: '¿Estás seguro?',
      text: "No podrás revertir estos cambios",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.proveedoresService.modificarProveedor(this.proveedorSeleccionado).subscribe(respose => {
          this.limpiarCajas();
          Swal.fire(
            '¡Modificado!',
            'El articulo ha sido modificado exitosamente',
            'success'
          )
          this.router.navigate(["articulos/"])
        });

        //this.msgModificar = true;
      }
    })
  }

  limpiarCajas() {
    this.proveedorSeleccionado = {
    }
  }

  cerrarAlert() {
    this.msgAlert = false;
    this.msgModificar = false;
  }
}
