import { Component, Inject, Injector, ViewChild } from '@angular/core';
import { SidebarComponent } from './sidebar/sidebar.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  @ViewChild("sideBar") sideBar : SidebarComponent | undefined;


  mostrarSideBar(){
    this.sideBar?.mostrarSideBar();
  }
}
