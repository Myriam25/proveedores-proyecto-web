import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TablaComponent } from './tabla/tabla.component';
import { FormularioComponent } from './formulario/formulario.component';

const routes :  Routes = [
  {path: 'articulos', component: TablaComponent},
  {path:  'agregararticulo', component:  FormularioComponent},
  {path: "modificarArticulo/:id", component: FormularioComponent},
  {path: '**', redirectTo : 'articulos'}
]

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
