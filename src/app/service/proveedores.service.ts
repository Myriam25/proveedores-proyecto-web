import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Proveedores } from '../Interfaces/Proveedores';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresService {
  proveedores: Proveedores[] = [
    {codigo: 1, razonsocial: "Carlos", direccion: "Prado Bonito", email: "Carlos@gmial.com",id: 44.00},
    {codigo: 2, razonsocial: "Isaac", direccion: "Prado Bonito", email: "isaac@gmial.com",id: 44.00},
    {codigo: 3, razonsocial: "karla", direccion: "club real", email: "karlita@gmial.com",id: 48.00},
    {codigo: 4, razonsocial: "myriam", direccion: "la reforma", email: "myriam@gmial.com",id: 47.00},
    {codigo: 5, razonsocial: "marco", direccion: "Prados del sol", email: "marcos@gmial.com",id: 44.00},
  ]

  baseURL:string="http://localhost:3000/api/proveedores/"

  constructor(private http : HttpClient) { }

  returnData() : Observable<Proveedores[]> {
    //return this.articulos;
    return this.http.get<Proveedores[]>(this.baseURL);
  }

  validacion(articulo: Proveedores): boolean{
    const busqueda = this.proveedores.filter(obj => obj.codigo == articulo.codigo);
    if(busqueda.length != 0){
      return true;
    }
    return false;
  }

  agregarProvedor(proveedor : Proveedores) : Observable<any> {

    return this.http.post(this.baseURL,proveedor);
  }

  getIndex(articulo : Proveedores): number{
    let index = 0;
    this.proveedores.forEach(art => {
      if(articulo.codigo == art.codigo){
        index = this.proveedores.indexOf(art);
      }
    });
    return index;
  }

  modificarProveedor(proveedores : Proveedores): Observable<Proveedores>{
    return this.http.put<Proveedores>(this.baseURL + proveedores.id, proveedores);
  }

  borrarArticulo(proveedores: Proveedores): Observable<Proveedores>{
      return this.http.delete<Proveedores>(this.baseURL + proveedores.id);

  }

  getProveedor(id:number):Observable<Proveedores>{
    return this.http.get<Proveedores>(this.baseURL + id);
  }
}
