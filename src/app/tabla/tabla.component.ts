import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { Proveedores } from '../Interfaces/Proveedores';
import {ProveedoresService} from '../service/proveedores.service';

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})

export class TablaComponent implements OnInit{
  proveedores : Proveedores[] = [];

  proveedorSeleccionado:  Proveedores={
  }

  @Output() seleccion = new EventEmitter();

  constructor(private proveedoresService :ProveedoresService, private router:Router){

  }
  ngOnInit() :void {
    //this.proveedors = this.proveedoresService.returnData();
    this.proveedoresService.returnData().subscribe((data)=>{
      console.log(data);
      this.proveedores = data;
    });
  }

  borrarProveedor(proveedor: Proveedores): void{
    this.proveedoresService.borrarArticulo(proveedor).subscribe(respose =>{
      this.ngOnInit();
    })
  }

  seleccionarProveedor(proveedor: Proveedores): void{
    this.proveedorSeleccionado = {
      ...proveedor
    }
    //this.seleccion.emit(this.proveedorSeleccionado);
    this.router.navigate(["modificarArticulo/" + proveedor.id])
  }
}
